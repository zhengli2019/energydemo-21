'''
    KDE Based Wind Roses
    ====================
    Schetch KDE Wind Roses that show climatological changes using the most 
    straightforward of the proposed designs. 
                                                                                 '''

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import xarray as xr
import numpy as np
from sklearn.neighbors import KernelDensity
from matplotlib import cm

_ = sns.set_style('darkgrid')
_ = sns.set_palette('deep')


def set_values(uwind_path,vwind_path,site=(0,53.5)) :
    ''' 
    Read in simulated values and output a table of wind direction and 
    speed for a given site.
    Note: Currently using the convention when converting to direction 
          in degrees of representing the direction that the wind is 
          blowing towards. This means can compare directly with the 
          vectors from the data set (often would be represented instead 
          as the direction that wind is blowing from).
   
    Variables
    ---------
    uwind_path : path to file containing u-dir wind vector values (str)
    vwind_path : path to file containing v-dir wind vector values (str)
    site : longitude and latitude pair giving location to analyse (str)

                                                                            '''
    # ... test vector match ...
    idx_u = xr.open_dataset(uwind_path).time.values
    idx_v = xr.open_dataset(vwind_path).time.values
    if not np.array_equal(idx_u,idx_v) :
        print(" +++ Chronology of u and v fields do not match +++")
        return()

    # ... set storage ...
    W = pd.DataFrame(
        index=pd.to_datetime(idx_u),
        columns=['u','v'])

    # ... read in vectors ...
    ''' 
    Different simulations will have have different names for the   
    longitude and latitude coordinates, will need to update command
    to use different labels as expand use of the script. 
                                                                          '''
    W['u'] = xr.open_dataset(uwind_path).uas.sel(
        rlat=site[1],rlon=site[0],method='nearest').values
    W['v'] = xr.open_dataset(vwind_path).vas.sel(
        rlat=site[1],rlon=site[0],method='nearest').values

    # ... wind speed in [km/hr] ...
    W['ws'] = np.sqrt(W.u**2 + W.v**2)*3.6

    # ... direction in degrees ...
    mod = 90 #270
    W['dir'] = (mod-(np.arctan2(W.v,W.u)*(180/np.pi)))%360

    # ... direction in radians
    W['theta'] = W.dir*(np.pi/180.0)

    return(W)

def create_wind_rose(Whist,Wproj,fig_name='test') :
    ''' 
    Draw the KDE wind roses

    Variables
    ---------
    Whist : table (produced by `set_values()` giving wind direction
            for the "historical period"
    Wproj : table (produced by `set_values()` giving wind direction
            for the "projection period"
    fig_name : desired file name for produced figure (str)

                                                                            '''
    cmap = sns.color_palette('Set2')
    months = ['Jan','Feb','Mar','Apr','May','Jun',
              'Jul','Aug','Sep','Oct','Nov','Dec']

    # ... create image ...
    _ = plt.figure(figsize=(16,14))

    for ix in range(12) : 
    
        chart = plt.subplot(3,4,(1+ix), projection='polar')
        # ... orient the theta coordinates to behave like a compass ...
        _ = chart.set_theta_zero_location("N")
        _ = chart.set_theta_direction(-1)
        _ = chart.grid(True)
        # ... label theta axis ...
        _ = chart.set_xticks(
            np.arange(0/180.*np.pi,360/180.*np.pi,45/180.*np.pi))
        _ = chart.set_xticklabels(
            ['N','NE','E','SE','S','SW','W','NW'],
            fontdict={'fontsize':12})

        ## Historical 
        # ... cyclic data for fitting ...
        #X = Whist.dir.values.copy()
        X = Whist.loc[
            [x.month == (1+ix) for x in Whist.index],'dir'].values.copy()
        Xa = 360+X[X<=180.0]
        Xb = -1*(360-X[X>180.0])
        XX = np.concatenate((Xa,X,Xb),axis=None) 
        # ... create a density estimator ...
        estm = KernelDensity(
            kernel='gaussian',bandwidth=10
        ).fit(XX.reshape(-1, 1))
        del([X,Xa,Xb,XX])
        # ... estimate density of subset ...
        x = np.arange(0,360.2,0.2)
        t = x*(np.pi/180.0)
        d = np.exp(
            estm.score_samples(
                x.reshape(-1, 1)))
        # ... join circle ...
        t = np.concatenate((t,t[0]),axis=None)
        d = np.concatenate((d,d[0]),axis=None)
        # ... draw rose ...
        lab = 'hist'
        _ = chart.plot(
            t,d,
            color=cmap[0],
            linewidth=4,
            alpha=0.8,
            label=lab)        

        ## Projection
        # ... cyclic data for fitting ...
        #X = Wproj.dir.values.copy()
        X = Wproj.loc[
            [x.month == (1+ix) for x in Whist.index],'dir'].values.copy()    
        Xa = 360+X[X<=180.0]
        Xb = -1*(360-X[X>180.0])
        XX = np.concatenate((Xa,X,Xb),axis=None) 
        # ... create a density estimator ...
        estm = KernelDensity(
            kernel='gaussian',bandwidth=10
        ).fit(XX.reshape(-1, 1))
        del([X,Xa,Xb,XX])
        # ... estimate density of subset ...
        x = np.arange(0,360.2,0.2)
        t = x*(np.pi/180.0)
        d = np.exp(
            estm.score_samples(
                x.reshape(-1, 1)))
        # ... join circle ...
        t = np.concatenate((t,t[0]),axis=None)
        d = np.concatenate((d,d[0]),axis=None)
        # ... draw rose ...
        lab = 'proj'
        _ = chart.plot(
            t,d,
            linestyle='dashed',
            color='black',
            linewidth=3,
            alpha=0.8,
            label=lab)        

        # ... describe radial axis ...
        _ = chart.set_ylim(0,0.01)
        _ = chart.set_yticks([0.005,0.01])
    
        # ... label month ...
        _ = plt.title(months[ix],loc='left',fontsize=18)

        # ... set labels ...
        if ix == 11 : 
            _ = plt.legend(loc=(0.75,-0.1),ncol=3)
    
    # ... output figure ...
    _ = plt.savefig(fig_name+'.png')

# ________________________________________________________________________
# ::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::

## Choose analysis site
site = (0,53.5)
## ID meteorolgical parameter names
metprm = ['vas','uas']
## Set path to data
datadir='/home/ubuntu/eucordex/NorthSea/'

## ID files
Usimlab_hist=[]
Vsimlab_hist=[]
Usimlab_proj=[]
Vsimlab_proj=[]

Usimlab_hist += grod.glob(
    datadir+'NorthSea_'+'uas'
    +'*'+'historical'+'*.nc')
Usimlab_hist.sort()
Vsimlab_hist += grod.glob(
    datadir+'NorthSea_'+'vas'
    +'*'+'historical'+'*.nc')
Vsimlab_hist.sort()
Usimlab_proj += grod.glob(
    datadir+'NorthSea_'+'uas'
    +'*'+'rcp85'+'*.nc')
Usimlab_proj.sort()
Vsimlab_proj += grod.glob(
    datadir+'NorthSea_'+'vas'
    +'*'+'rcp85'+'*.nc')
Vsimlab_proj.sort()


## Generate data tables
for x in range(0, len(Usimlab_hist)):
	print('processing ', Usimlab_hist[x])
	Whist= set_values(
    		uwind_path=Usimlab_hist[x],
    		vwind_path=Vsimlab_hist[x],
    		site=site)
	Wproj= set_values(
    		uwind_path=Usimlab_proj[x],
    		vwind_path=Vsimlab_proj[x],
    		site=site)

## Draw figure
	create_wind_rose(
    		Whist=Whist,
    		Wproj=Wproj,
    		fig_name=('_'.join(Usimlab_hist[x].split('_')[1:4])
         	+'_'+'_'.join(Usimlab_hist[x].split('_')[6:9])
			+'_test_figure'))


